# Do
```bash
npm install
npm run dev
npm run build
npm run preview
```

# Wip
* **Figma**
* Make Pixel Art of [Bufficorn](https://images.squarespace-cdn.com/content/v1/614b990e002ad146a5478e8b/1633629568660-ZQ2SYA2991APNC4C7QI6/gwei.png?format=750w)
* Use MagicaVoxel to build a 3D bufficorn
* SetUp Ink!
* UsePolkadotJs to interact
* Print it



# Vue 3 + Typescript + Vite

This template should help get you started developing with Vue 3 and Typescript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)

## Type Support For `.vue` Imports in TS

Since TypeScript cannot handle type information for `.vue` imports, they are shimmed to be a generic Vue component type by default. In most cases this is fine if you don't really care about component prop types outside of templates. However, if you wish to get actual prop types in `.vue` imports (for example to get props validation when using manual `h(...)` calls), you can enable Volar's `.vue` type support plugin by running `Volar: Switch TS Plugin on/off` from VSCode command palette.
