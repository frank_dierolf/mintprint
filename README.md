# MintPrint

**[Buidl](https://hackerlink.io/buidl/2055)**</br>
**[Presentation - Youtube](https://youtu.be/-h1a8qrllLg)** </br>
**[3DModel - Click me](https://ibb.co/qkv6KMz)**</br>
**[3DPrint- Click Me](https://ibb.co/Wnzbzk7)**</br>
**[*3DNFT - IPFS*](https://mintprint.mypinata.cloud/ipfs/QmdEtfX3pgrxWJ7RsEUMGu7D4G3bSccVp5oF9A5Nd3EhWW?object=QmdEtfX3pgrxWJ7RsEUMGu7D4G3bSccVp5oF9A5Nd3EhWW&filename=3DNFTBufficorn.glb)**</br></br>
Mint and Print is selfexplaining.</br>
Its place where people can mint and print their 3D Models.</br></br>
**Problem**</br>
In the field of Engineering there is no good way to recognize who created/owns which digital property. </br></br>
**Solution**</br>
We give every 3D Model a simple NFT, which makes clear which rights and permission does who has.</br></br>

#Team<3
**Frank** *Web3(XR) Developer* | Loves Holograms</br> 
**Saro** *Buisness Developer* | Loves Art,Crypto&Commerce</br>
**Watson** *Marketer* | Loves People&Impact</br>
**Stephan** *3DPrinting Engineer* | Loves decentralised Production
